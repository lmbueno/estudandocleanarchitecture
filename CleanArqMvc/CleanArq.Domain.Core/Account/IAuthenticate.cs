﻿using System.Threading.Tasks;

namespace CleanArqMvc.Domain.Core.Account
{
    public interface IAuthenticate
    {
        Task<bool> Authenticate(string email, string password);
        Task<bool> RegisterUser(string email, string password);

        Task Logout();
    }
}
