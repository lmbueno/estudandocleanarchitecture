﻿namespace CleanArqMvc.Domain.Core.Account
{
    public interface ISeedUserRoleInitial
    {
        void SeedUsers();
        void SeedRoles();
    }
}
