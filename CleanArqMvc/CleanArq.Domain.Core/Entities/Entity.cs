﻿
namespace CleanArqMvc.Domain.Core.Entities
{
    public abstract class Entity
    {
        public int Id { get; protected set; }
    }
}
