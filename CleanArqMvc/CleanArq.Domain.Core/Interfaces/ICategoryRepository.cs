﻿using CleanArqMvc.Domain.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArqMvc.Domain.Core.Interfaces
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> GetCategoriasAsync();
        Task<Category> GetByIdAsync(int? id);
        Task<Category> CreateAsync(Category category);
        Task<Category> UpdateAsync(Category category);
        Task<Category> RemoveAsync(Category category);
    }
}
