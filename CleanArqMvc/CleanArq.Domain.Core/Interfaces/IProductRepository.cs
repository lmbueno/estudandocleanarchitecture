﻿using CleanArqMvc.Domain.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArqMvc.Domain.Core.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetProductAsync();
        Task<Product> GetByIdAsync(int? id);
        //Task<Product> GetproductCategoryAsync(int? idcategoria);
        Task<Product> CreateAsync(Product product);
        Task<Product> UpdateAsync(Product product);
        Task<Product> RemoveAsync(Product product);
    }
}
