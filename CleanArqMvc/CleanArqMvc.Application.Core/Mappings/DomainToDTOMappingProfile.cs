﻿using AutoMapper;
using CleanArqMvc.Application.Core.DTOs;
using CleanArqMvc.Domain.Core.Entities;

namespace CleanArqMvc.Application.Core.Mappings
{
    public class DomainToDTOMappingProfile : Profile
    {
        public DomainToDTOMappingProfile()
        {
            CreateMap<Category, CategoryDTO>().ReverseMap();
            CreateMap<Product, ProductDTO>().ReverseMap();
        }
    }
}
