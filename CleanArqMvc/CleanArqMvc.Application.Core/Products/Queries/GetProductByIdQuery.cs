﻿using CleanArqMvc.Domain.Core.Entities;
using MediatR;

namespace CleanArqMvc.Application.Core.Products.Queries
{
    public class GetProductByIdQuery : IRequest<Product>
    {
        public int Id { get; set; }

        public GetProductByIdQuery(int id)
        {
            Id = id;
        }
    }
}
