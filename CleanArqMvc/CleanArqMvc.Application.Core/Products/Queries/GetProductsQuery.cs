﻿using CleanArqMvc.Domain.Core.Entities;
using MediatR;
using System.Collections.Generic;

namespace CleanArqMvc.Application.Core.Products.Queries
{
    public class GetProductsQuery : IRequest<IEnumerable<Product>>
    {
    }
}
