﻿using AutoMapper;
using CleanArqMvc.Application.Core.DTOs;
using CleanArqMvc.Application.Core.Interfaces;
using CleanArqMvc.Application.Core.Products.Commands;
using CleanArqMvc.Application.Core.Products.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArqMvc.Application.Core.Services
{
    public class ProductService : IProductService
    {
        private IMediator _mediator;
        private IMapper _mapper;
        public ProductService(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task<ProductDTO> GetById(int? id)
        {
            var productByIdQuery = new GetProductByIdQuery(id.Value);

            if (productByIdQuery == null)
                throw new Exception($"Entity could not be loaded.");

            var result = await _mediator.Send(productByIdQuery);

            return _mapper.Map<ProductDTO>(result);
        }

        public async Task<ProductDTO> GetProductCategory(int? id)
        {
            var productByIdQuery = new GetProductByIdQuery(id.Value);

            if (productByIdQuery == null)
                throw new Exception($"Entity could not be loaded.");

            var result = await _mediator.Send(productByIdQuery);

            return _mapper.Map<ProductDTO>(result);
        }

        public async Task<IEnumerable<ProductDTO>> GetProducts()
        {
            var productQuery = new GetProductsQuery();

            if (productQuery == null)
                throw new Exception($"Error could not be found.");

            var result = await _mediator.Send(productQuery);

            return _mapper.Map<IEnumerable<ProductDTO>>(result);
        }

        public async Task Add(ProductDTO productDTO)
        {
            var productEntity = _mapper.Map<ProductCreateCommand>(productDTO);

            await _mediator.Send(productEntity);
        }

        public async Task Update(ProductDTO productDTO)
        {
            var productEntity = _mapper.Map<ProductUpdateCommand>(productDTO);

            await _mediator.Send(productEntity);
        }

        public async Task Remove(int? id)
        {
            var productEntity = new GetProductByIdQuery(id.Value);

            if (productEntity == null)
                throw new Exception($"Entity could not be loaded.");

            await _mediator.Send(productEntity);
        }
    }
}
