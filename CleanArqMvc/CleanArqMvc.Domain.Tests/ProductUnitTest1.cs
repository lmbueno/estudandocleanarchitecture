﻿using CleanArqMvc.Domain.Core.Entities;
using FluentAssertions;
using System;
using Xunit;

namespace CleanArqMvc.Domain.Tests
{
    public class ProductUnitTest1
    {
        [Fact]
        public void CreateProduct_WithValidParameters_ResultObjectValidState()
        {
            Action action = () => new Product(1, "Product Name", "Product Description", 9.99m, 99, "product image");

            action.Should()
                .NotThrow<CleanArqMvc.Domain.Core.Validation.DomainExceptionValidation>();
        }

        [Fact]
        public void CreateProduct_NegativeIdValue_DomainExceptionInvalidId()
        {
            Action action = () => new Product(-1, "Product Name", "Product Description", 9.99m, 99, "product image");

            action.Should()
                .Throw<CleanArqMvc.Domain.Core.Validation.DomainExceptionValidation>()
                .WithMessage("Invalid Id Value.");
        }

        [Fact]
        public void CreateProduct_ShortNameValue_DomainExceptionShortName()
        {
            Action action = () => new Product(1, "Pr", "Product Description", 9.99m, 99, "product image");

            action.Should()
                .Throw<CleanArqMvc.Domain.Core.Validation.DomainExceptionValidation>()
                .WithMessage("Invalid name, too short, minimum 3 caracteres.");
        }

        [Fact]
        public void CreateProduct_LongImageNmae_DomainExceptionLongName()
        {
            Action action = () => new Product(1, "Product Name", "Product Description", 9.99m, 99, "product image toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                "oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");

            action.Should()
                .Throw<CleanArqMvc.Domain.Core.Validation.DomainExceptionValidation>()
                .WithMessage("Invalid image name, too long, maximum 250 caracteres.");
        }
    }
}
