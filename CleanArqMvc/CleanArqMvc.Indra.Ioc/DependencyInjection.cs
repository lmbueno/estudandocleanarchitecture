﻿using CleanArqMvc.Application.Core.Interfaces;
using CleanArqMvc.Application.Core.Mappings;
using CleanArqMvc.Application.Core.Services;
using CleanArqMvc.Domain.Core.Account;
using CleanArqMvc.Domain.Core.Interfaces;
using CleanArqMvc.Infra.Data.Core.Context;
using CleanArqMvc.Infra.Data.Core.Identity;
using CleanArqMvc.Infra.Data.Core.Repositories;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CleanArqMvc.Infra.IoC.Core
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfraStructure(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
            b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

            service.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            service.ConfigureApplicationCookie(options => 
                                               options.AccessDeniedPath = "/Account/Login");

            service.AddScoped<ICategoryRepository, CategoryRepository>();
            service.AddScoped<IProductRepository, ProductRepository>();
            service.AddScoped<IProductService, ProductService>();
            service.AddScoped<ICategoryService, CategoryService>();
            service.AddAutoMapper(typeof(DomainToDTOMappingProfile));
            service.AddScoped<IAuthenticate, AuthenticateServices>();
            service.AddScoped<ISeedUserRoleInitial, SeedUserRoleInitialServices>();

            var myHandlers = AppDomain.CurrentDomain.Load("CleanArqMvc.Application.Core");
            service.AddMediatR(myHandlers);

            return service;
        }
    }
}
